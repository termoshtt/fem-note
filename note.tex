\documentclass[a4paper]{ltjsarticle}

\usepackage[unicode]{hyperref}
\usepackage{graphicx}
\usepackage{amsmath}
\usepackage{amssymb}
\usepackage{color}
\usepackage{cite}
\usepackage{cleveref}

\newcommand\R{\mathbb{R}}
\newcommand\rotate[1]{
\begin{pmatrix}
  \cos{\theta} & \sin{\theta} \\
 -\sin{\theta} & \cos{\theta} \\
\end{pmatrix}
}
\newcommand\rotatem[1]{
\begin{pmatrix}
  \cos{\theta} & -\sin{\theta} \\
  \sin{\theta} &  \cos{\theta} \\
\end{pmatrix}
}

\title{FEM勉強会ノート}
\author{Toshiki Teramura (\href{https://twitter.com/termoshtt}{@termoshtt})}
\begin{document}
\maketitle

\begin{abstract}
  このノートは @termoshtt が\href{https://ricos.connpass.com/event/125855/}{非線形有限要素法勉強会} に参加した後に
  雰囲気で中身をおさらいするための資料である。
  この内容は勉強会の内容と特に整合しない、俺の考える最高でクールなFEMの導入だぜ!
\end{abstract}

\section{Notations}
実数を $\R$ と書き、ユークリッド空間を $\R^N$ と書く。
ユークリッド空間の元も太字で書いたりはせずに $x \in \R^N$ のように通常の文字で書く。
またこの時 $x_i$ と書いたらそれは成分の実数値を表す。
どの座標系での成分値なのかは文脈から判断する必要がある。
また $x = [x_i]{}_i$ は例えば3次元ユークリッド空間なら $x = (x_1, x_2, x_3)$ と同等の意味を持つ略記法とする。

実数値の$N$次元正方行列を $M_N(\R)$ と書く。
$A \in M_N(\R)$に対しその成分表示を同様に $A = [a_{ij}]{}_{ij}$ のように略記する。
また特に明示する必要が無いときは $A = [a_{ij}]$ のようにさらに簡略化する。
この時、行列ベクトル積 $Ax$ (ただし $A = [a_{ij}]{}_{ij} \in M_N(\R)$, $x \in \R^N$)
は $[a_{ij}x_j]{}_i$ のように書ける ($j$については和を取る)。
高階のテンソル値についても同様の略記方法を採用する。

\section{弾性体}
\begin{figure}[ht]
  \centering
  \includegraphics{./diagram/elastic.pdf}
  \caption{壁に固定された弾性体を考えよう}\label{fig:elastic}
\end{figure}
弾性体とは外力を受けて変形した際に、力の働いていないときの状態へ戻ろうとする力（復元力と呼ぶ）を伴う物体の事を指す。
特に与えられた変形に対して線形に復元力が強くなる弾性体を線形弾性体、あるいはフック弾性体と呼ぶ。
例として\cref{fig:elastic}のように壁に弾性体が固定されいてる場合を考える。
例えば靴底のゴムのように比較的固い、しかし力を入れると変形するような素材を思い浮かべて欲しい。
この弾性体に外から力を加えると、
ちょうどばねが引かれた強さに応じて伸びるように、
釣り合う力が発生するように変形が生じる (\cref{fig:bended})。
この変形がどのように起こるを明らかにすることが弾性体理論の目的となる。
\begin{figure}[ht]
  \centering
  \includegraphics{./diagram/bended.pdf}
  \caption{圧力には勝てなかったよ😢}\label{fig:bended}
\end{figure}

議論の準備として状況を定義しておこう。
弾性体はユークリッド空間 (2次元$\R^2$あるいは3次元空間$\R^3$、一般に$\R^N$と書く) 上に存在し、
弾性体が変形前に占めている領域を $\Omega \subset \R^N$ と書くことにする。
$\Omega$は単連結であって、破断のように分裂する場合は考えない。
\begin{figure}[tb]
  \centering
  \includegraphics[width=0.45\linewidth]{./diagram/region.pdf}
  \includegraphics[width=0.45\linewidth]{./diagram/region_bended.pdf}
  \caption{変形前の弾性体の領域を$\Omega$、変形後の領域を$\Omega^\prime$と呼ぶ}\label{fig:regions}
\end{figure}
変形後に物体が占める領域を$\Omega^\prime$ と書く。
$\Omega$, $\Omega^\prime$共に境界は区分的になめらかだとする。
物体が変形の前後で欠損せず変形がなめらかだったとすると、これらの間には一対一に対応できるはずである。
変形前の点 $x \in \Omega$ が変形後の点 $\xi \in \Omega^\prime$ に対応するときに
$\xi = \phi(x)$ となるように関数 $\phi: \Omega \to \Omega^\prime$ を定める。
これは変形のなめらかさに由来して、微分同相写像になると期待できる。
つまり逆関数 $\psi = \phi^{-1}: \Omega^\prime \to \Omega$ が存在し、
$\phi$, $\psi$ が共に微分可能で微分係数の行列
$[\partial \phi_i / \partial x_j]{}_{ij}$
及び
$[\partial \psi_i / \partial \xi_j]{}_{ij}$
が全ての点で正則だと仮定する。
弾性体の変形はこの関数を一つ決めると完全に定まる。

\subsection{応力と力のつり合い}
次に弾性体が静止しているときの力のつり合いについて考えよう。
弾性体では連続に材質が分布していると仮定するので、
働く力や密度も同様に連続的に分布すると仮定する。
破断や衝撃波のようにこの仮定が破綻するには様々な理由があるが、
それぞれ連続な場合をベースにして個別に考えるので、まずは連続な場合を記述する。

力のつり合いはニュートンの質点に関する運動方程式を基礎としている。
これを弾性体のような連続体に対して拡張するには、
ある微小な領域を考え、その中の物質に対して働く力を考える。
力も連続に分布していると仮定しているので、
十分に微小な領域$V \subset \R^N$を考えることで均一な力が働いているように見なせる。
まず二次元の場合にこの$V$として長方形を選んだ場合を考え、その後に一般の領域を選んだ場合を考えよう。
\begin{figure}[tb]
  \centering
  \includegraphics{./diagram/stress_tensor.pdf}
  \caption{微小領域$V$に働く力}\label{fig:stress_tensor}
\end{figure}
この微小領域$V$に働く図を図示したのが\cref{fig:stress_tensor}である。
$V$はちょうど$X$, $Y$軸の方向を向いているとする (そうでない場合は後で考える)。
4つの面に対してそれぞれ働く力が図示してあるが、力は斜めに働く場合もあるのでそれぞれ成分に分解して考える。
今、力は均一に働いてるとみなせるような微小領域を考えているため、
力$F_{xx}$や$F_{yx}$は$X$軸方向の幅$\delta x = x^\prime - x$に、
$F_{xy}$や$F_{yy}$は$Y$軸方向の幅$\delta y = y^\prime - y$に比例する：
\begin{align*}
  F_{xx} = f_{xx} \delta x,\\
  F_{yx} = f_{yx} \delta x,\\
  F_{xy} = f_{xy} \delta y,\\
  F_{yy} = f_{yy} \delta y.
\end{align*}
この力の密度は位置$(x, y)$よって決まる量であり、
微小領域の大きさに比例する$F_{xy}$などと異なり大きさに依存しない。
弾性体は静止しているので、特にこの領域も静止しており、よってこれらの力はつりあっているはずである。
\begin{align*}
  (f_{xx} + f_{xx}^\prime)\delta x + (f_{xy} + f_{xy}^\prime)\delta y = 0, \\
  (f_{yx} + f_{yx}^\prime)\delta x + (f_{yy} + f_{yy}^\prime)\delta y = 0.
\end{align*}
$f_{xx}$等は$\delta x$, $\delta y$の取り方によらない量なので、
\begin{align*}
  f_{xx} = - f_{xx}^\prime, \\
  f_{xy} = - f_{xy}^\prime, \\
  f_{yx} = - f_{yx}^\prime, \\
  f_{yy} = - f_{yy}^\prime.
\end{align*}
が成り立つ。
よって静止している弾性体中で$V$を十分小さい長方形にとる場合において、$V$に働く力は$f_{xx}, f_{xy}, f_{yx}, f_{yy}$の4つの量で記述される。

\begin{figure}[tb]
  \centering
  \includegraphics{./diagram/stress_tensor_inclind.pdf}
  \caption{微小領域$V$が傾いている場合。$\theta$は傾きを表す (この図が$\theta < 0$の場合を表すようにとる)}\label{fig:stress_tensor_inclined}
\end{figure}
次に$V$が座標軸に対して傾いている場合を考えよう (\cref{fig:stress_tensor_inclined})。
$V$に合わせて傾けた座標系を$X^\prime$-$Y^\prime$のように表記すると、
この座標系では上の議論が成り立つので、
力が$V$に働く力は$f_{x^\prime x^\prime }, f_{x^\prime y^\prime }, f_{y^\prime x^\prime }, f_{y^\prime y^\prime }$
の4つの量で表現される。
これを元の座標系$X$-$Y$で見た場合とどう関係するのかを考察することで、
$f$が幾何学的にどのようにふるまうかを理解することが出来る。
表記が少しややこしいが、一つ目の添え字は力のベクトルの成分を表し、
二つ目の添え字は働いてる面の方向を表す。
図はややこしくなるので各面に働いている力をベースになる座標系で表示したときの値$F_{x x^\prime}$等
だけを表示している。$F_{x^\prime x^\prime}$等は\cref{fig:stress_tensor}をそのまま回転させたものに対応する。

働く力自体はベクトル値なので回転行列で次のように変換される:
\begin{align*}
  \begin{pmatrix}
    F_{x x^\prime} \\
    F_{y x^\prime} \\
  \end{pmatrix}
  &=
  \rotatem{\theta}
  \begin{pmatrix}
    F_{x^\prime x^\prime} \\
    F_{y^\prime x^\prime} \\
  \end{pmatrix}, \\
  \begin{pmatrix}
    F_{x y^\prime} \\
    F_{y y^\prime} \\
  \end{pmatrix}
  &=
  \rotatem{\theta}
  \begin{pmatrix}
    F_{x^\prime y^\prime} \\
    F_{y^\prime y^\prime} \\
  \end{pmatrix}.
\end{align*}
ここで$V$に働く力は体積に比例しているのではなく面の幅に依存している事に気を付けると、
$X$, $Y$方向に射影した幅
\begin{align*}
  \begin{pmatrix}
    \delta x \\
    \delta y \\
  \end{pmatrix}
  &=
  \rotatem{\theta}
  \begin{pmatrix}
    \delta x^\prime \\
    \delta y^\prime \\
  \end{pmatrix}
\end{align*}
を使って$V$に働く力のそれぞれの成分が二通りにかける:
\begin{align*}
  f_{xx} \delta x + f_{xy} \delta y = F_{xx^\prime} + F_{xy^\prime}, \\
  f_{yx} \delta x + f_{yy} \delta y = F_{yx^\prime} + F_{yy^\prime}.
\end{align*}

\end{document}
